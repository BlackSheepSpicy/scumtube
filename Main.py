#! /usr/bin/env python3
from __future__ import unicode_literals
import youtube_dl, os, json, YoutubeUploader, logging, smtplib, ssl, email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from bs4 import BeautifulSoup
import moviepy.editor as mov
from sys import argv,path,stdout
from shutil import rmtree
from string import Template
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from time import sleep

config={}
with open('config.json') as f:
	config=json.load(f)

#LOAD VIDEO NAME TEXT OPTIONS
font=config['Video Text']['Font']
size=config['Video Text']['Size']
color=config['Video Text']['Color']
backColor=config['Video Text']['Highlight']
stroke=config['Video Text']['Outline']
swidth=config['Video Text']['Outline Width']
position=('center','bottom')

#LOG CONFIG
logging.basicConfig(filename='ScumTubeTwitch.txt',filemode='a',level=logging.DEBUG)
logging.getLogger().addHandler(logging.StreamHandler())

#LOAD LOG FORWARDING OPTIONS
server=config['Logging']['SMTP Server']
port=config['Logging']['SMTP Port']
receiver=config['Logging']['Receiver']
sender=''
password=''
with open(config['Secrets']['Account Info']) as f:
	mail=json.load(f)
	sender=mail['Account Info']['Email']
	password=mail['Account Info']['Password']

#Method to handle email notifications
def send(subject,body='',attachment=''):
	message=MIMEMultipart()
	message['From']=sender
	message['To']=receiver
	message['Subject']=subject
	message['Bcc']=receiver
	message.attach(MIMEText(body,"plain"))
	
	if not attachment=='':
		with open(attachment,'rb') as f:
			part=email.mime.base.MIMEBase('application','octet-stream')
			part.set_payload(f.read())
		email.encoders.encode_base64(part)
		part.add_header("Content-Disposition","attachment; filename="+attachment)
		message.attach(part)
	with smtplib.SMTP(server,port) as s:
		s.starttls()
		s.login(sender, password)
		s.sendmail(sender,receiver,message.as_string())

def main():
	category=argv[1].replace('%20',' ')
	send('Video Job Started: '+category)
	
	#Uses headless instance of FireFox (selenium with geckodriver) to get the JS rendered clip page, then uses bs4 to make a list of clip metadata
	while True:	
		logging.info('Starting clip scrape of category: '+category)
		url=f'https://www.twitch.tv/directory/game/{argv[1]}/clips?range={argv[2]}'
		options=Options()
		options.set_headless(True)
		browser=webdriver.Firefox(options=options)
		browser.get(url)
		html=browser.execute_script("return document.body.innerHTML")
		browser.quit()
		logging.info('retrieved raw HTML, refining')

		soup=BeautifulSoup(html,'html.parser')
		clips=soup.find_all('a',{'class': 'tw-interactive tw-link tw-link--button tw-link--hover-underline-none tw-link--inherit'})	
		logging.debug(clips)
		if clips:
			logging.info('done')
			break
		else:
			logging.warning('clip scrape failed, retrying in 5 seconds')
			browser.quit()
			sleep(5)
			continue

	#Goes through each entry in the metadata list gathered prior to download each clip, overlays the name of the streamer onto the clip, then adds clip object to list
	#NOTE: 29 FPS clips are known to make moviepy throw a bitchfit and are handled here, have had errors with very few clips but havent been able to track the issue.
	logging.info('beginning clip download/processing')
	finalDuration=0
	creators=[]
	vidList=[]
	i=0
	for clip in clips:
		print(clip)
		if '/clip/' in clips[i]['href']:
			ydl_opts = {'outtmpl': f'ClipCache/{i}.%(ext)s'}
			with youtube_dl.YoutubeDL(ydl_opts) as ydl:
				info=ydl.extract_info('https://twitch.tv'+clips[i]['href'], download=False)
				logging.info(info)
				if info.get('fps')==29:
					logging.warning('bad clip, skipping')
					i+=1		
					continue
				video=ydl.download(['https://twitch.tv'+clips[i]['href']])			
				twitch=mov.VideoFileClip(f'ClipCache/{i}.mp4').resize((1280,720))
				finalDuration+=twitch.duration
				name=clips[i+1].getText()
				if '(' in name:
					name=name[name.find('(')+1:].strip(')')
				if name not in creators:
					creators.append(name)
				nameLayer=mov.TextClip(name, font=font,fontsize=size, color=color, bg_color=backColor, stroke_color=stroke, stroke_width=swidth).set_pos(position).set_duration(twitch.duration)
				vidList.append(mov.CompositeVideoClip([twitch, nameLayer]))	
		i+=1
	rmtree('ClipCache')
	logging.info('done')

	#YOUTUBE METADATA PREP
	#Makes youtube API call to find current count of videos that exist for this series (used to number video)
	googleAPIClient=config['Secrets']['Client Info']
	accountInfo=config['Secrets']['Account Info']
	logging.info('loading API client info')
	with open(googleAPIClient) as f:
		clientData=json.load(f)
		clientID=clientData['installed']['client_id']
		clientSecret=clientData['installed']['client_secret']
		tokenURI=clientData['installed']['token_uri']
	logging.info('loading Account info')
	with open(accountInfo) as f:
		accountData=json.load(f)
		refreshToken=accountData['Tokens']['Refresh Token']
	
	#Loads title template in from config json and applies necessary attributes
	logging.info('starting metadata prep')
	timeFrame='of'	
	if argv[2]=='1d':
		timeFrame+=' the Day'
	elif argv[2]=='7d':
		timeFrame+=' the Week'
	elif argv[2]=='30d':
		timeFrame+=' the Month'
	elif argv[2]=='all':
		timeFrame+=' All Time'
	
	#queries existing videos to determine number of video in series, if none found, will default to first video
	credentials=Credentials(
		None,
		refresh_token=refreshToken,
		token_uri=tokenURI,
		client_id=clientID,
		client_secret=clientSecret
	)
	youtube=build('youtube','v3',credentials=credentials)
	videoRequest=youtube.search().list(
		q=category+' '+timeFrame,
		part='snippet',
		forMine=True,
		order='date',
		maxResults=50,
		type='video'
	)
	videoResponse=videoRequest.execute()	
	print(videoResponse)
	vidNum=int(videoResponse['pageInfo']['totalResults'])+1


	keywords=dict(category=category,timeFrame=timeFrame,vidNum=vidNum)
	titleRaw=Template(config['Video Metadata']['Title Template'])
	title=titleRaw.substitute(keywords)
	logging.info('Video Title: '+title)
	
	#Dynamically creates description including links to all streamers featured.
	#Info sourced from previous html scrape
	description='Featured Streamers:\n'
	for name in creators:
		description=description+name+': '+'https://twitch.tv/'+name+'\n'
	logging.info('Video Description:\n'+description)
	
	#Creates list of tag words using streamer names, the twitch category, and custom tags sourced from the config file	
	tags=config['Video Metadata']['Tags']+creators	
	tags.append(category)
	
	youtubeCategory=config['Video Metadata']['Default Category']
	
	vidStatus='private'
	if len(argv)==4:
		vidStatus=argv[3]

	logging.info('Clip Count: '+str(len(vidList)))
	logging.info('Video Duration: '+str(int(finalDuration/60))+':'+str(int(finalDuration%60)))

	#Passes list of processed clips to moviepy where they are appended together and rendered
	#NOTE: This shit takes forever, moviepy doesent support GPU acceleration either
	logging.info('Starting render')
	mov.concatenate_videoclips(vidList).write_videofile('complete.mp4',fps=30,audio_codec='aac')

	YoutubeUploader.upload('complete.mp4', googleAPIClient, accountInfo, title=title, description=description, tags=tags, categoryName=youtubeCategory, privacyStatus=vidStatus)
	
	os.system('mkdir STTLog')
	os.system('mv ScumTubeTwitch.txt STTLog/ScumTubeTwitch.txt')
	os.system('mv geckodriver.log STTLog/geckodriver.log')
	os.system('zip -r logs.zip STTLog')
	send('Video Successfully Posted: '+title,attachment='logs.zip')	
	os.system('make clean')

#if render run fails, falls back to notifying log handler of failure with attached logs then cleans up
try:
	main()
except Exception as e:
	print(e)
	os.system('mkdir STTLog')
	os.system('mv ScumTubeTwitch.txt STTLog/ScumTubeTwitch.txt')
	if os.path.exists('geckodriver.log'):
		os.system('mv geckodriver.log STTLog/geckodriver.log')
	os.system('zip -r logs.zip STTLog')
	send('Video Job Failed',str(e),'logs.zip')
	os.system('make clean')
