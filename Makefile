install:
	install/DebianInstall.sh

nuke:
	- sudo userdel -r twitch
	- sudo rm credentials/*.cred
	- sudo rm -r data
	- sudo rm config.json

clean:
	- rm *.log
	- rm *.txt
	- rm -r ClipCache
	- rm -r __pycache__
	- rm *.mp4
	- rm -r STTLog
	- rm logs.zip
deploy:
