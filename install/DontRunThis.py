import os, json, getpass, smtplib
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
config={}
dataPath=""
credPath=""
with open('install/config.bak') as f:
	config=json.load(f)
	dataPath=config['File Paths']['Data']
	credPath=config['File Paths']['Credentials']

if not os.path.exists(credPath):
	os.mkdir(credPath)
	print('created credentials file')

#check for google API client info
clientInfo=''
while True:
	for f in os.listdir(credPath):
		if '.apps.googleusercontent.com.json' in f:
			clientInfo=credPath+f
			print('found Google API client info file: '+f)
			with open(clientInfo) as f:
				clientData=json.load(f)
				clientID=clientData['installed']['client_id']
				clientSecret=clientData['installed']['client_secret']
				tokenURI=clientData['installed']['token_uri']
			break
	else:
		input('Google API client info not found, load from google developer console into credentials file then press anything to continue')
		continue
	break

#construct credentials file
accountName=input('name of YouTube account: ')
accountFile=accountName+'.cred'
if not os.path.exists(credPath+accountFile):
	print('account information not found, creating')
	scopes = [
		"https://www.googleapis.com/auth/youtube.upload",
		"https://www.googleapis.com/auth/youtube.readonly"
	]
	while True:	
		email=input('Account Email: ')
		password=getpass.getpass('Password: ')
		try:
			with smtplib.SMTP(config['Logging']['SMTP Server'],config['Logging']['SMTP Port']) as s:
				s.starttls()				
				s.login(email,password)
			break
		except smtplib.SMTPAuthenticationError:
			print('Invalid Credententials')
			continue
	
	flow = InstalledAppFlow.from_client_secrets_file(clientInfo, scopes)
	token = flow.run_console()
	info = {
		'Account Info':{
			'Email':email,
			'Password':password
			},
		'Tokens':{'Refresh Token': token.refresh_token}
	}
	with open(credPath+accountFile,'w') as f:		
		json.dump(info,f,indent=4)
	print('account file successfully created')
else:
	print('found account info file')

if config['Logging']['Receiver']=="":
	config['Logging']['Receiver']=input('name of log email receiver: ')
else:
	print('log email reciever: '+config['Logging']['Receiver'])
config['Secrets']['Client Info']=clientInfo[2:]
config['Secrets']['Account Info']=credPath[2:]+accountFile
with open('config.json','w') as f:
	json.dump(config,f,indent=4)
print('Setup Complete')
