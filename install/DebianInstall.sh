#!/bin/sh
if ! [ $(id -u) = 0 ] 
then
   echo "ERROR: This script needs to run as root"
   exit 1
fi
apt -y install imagemagick ffmpeg python3-pip firefox-esr zip
if ! grep -q "<\!-- <policy domain=\"path\" rights=\"none\" pattern=\"@\*\"\/> -->" /etc/ImageMagick-6/policy.xml
then
	sed -i '/<policy domain="path" rights="none" pattern="@\*"\/>/s/^/<!-- /' /etc/ImageMagick-6/policy.xml
	sed -i '/<policy domain="path" rights="none" pattern="@\*"\/>/s/$/ -->/' /etc/ImageMagick-6/policy.xml
else
	echo "ImageMagick policy file already edited"
fi
if ! [ -e /usr/bin/geckodriver ]
then
	wget https://github.com/mozilla/geckodriver/releases/download/v0.25.0/geckodriver-v0.25.0-linux64.tar.gz
	tar xzvf geckodriver-v0.25.0-linux64.tar.gz
	rm geckodriver-v0.25.0-linux64.tar.gz
	sudo mv geckodriver /usr/local/bin
fi
pip3 install selenium
pip3 install bs4
pip3 install youtube_dl
pip3 install moviepy
pip3 install google-api-python-client
pip3 install google-auth-oauthlib
echo "LINUX USER"
adduser twitch
mkdir credentials
python3 install/DontRunThis.py
chown -R twitch:twitch .
