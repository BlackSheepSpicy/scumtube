#!/usr/bin/env python3
import json, os, socket, time
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.http import MediaFileUpload
from googleapiclient.errors import HttpError

def upload(vidFile, clientInfo, tokenFile='', title=time.gmtime(), description='', tags=[], categoryName='Entertainment', privacyStatus='private'):
	print('upload called')
	#loads client info from json file, available from google developer terminal
	try:
		print('loading client info')
		clientData=json.load(open(clientInfo))
		clientID=clientData['installed']['client_id']
		clientSecret=clientData['installed']['client_secret']
		tokenURI=clientData['installed']['token_uri']
		print('done')
	except IOError:
		print('client information file not found, generate json on google dev console')
		exit()
	#loads token info from json file, if none exists, oauth kicks and one will be generated
	print('loading token data')
	if tokenFile=='':
		scopes = [
		"https://www.googleapis.com/auth/youtube.upload",
		"https://www.googleapis.com/auth/youtube.readonly"
		]
		flow=InstalledAppFlow.from_client_secrets_file(clientInfo, scopes)
		resp=flow.run_console()
		refreshToken=resp.refresh_token 
	else:
		with open(tokenFile) as f:
			tokens=json.load(f)
			refreshToken=tokens['Tokens']['Refresh Token']
		print('done')

	#generates credentials to shoot at google
	credentials=Credentials(
		None,
		refresh_token=refreshToken,
		token_uri=tokenURI,
		client_id=clientID,
		client_secret=clientSecret
	)
	youtube=build('youtube','v3',credentials=credentials)
	
	videoCategoriesRequest=youtube.videoCategories().list(part='snippet',regionCode='US')
	videoCategories=videoCategoriesRequest.execute()	
	categoryID='0'
	i=0
	for i in range(len(videoCategories['items'])):
		if videoCategories['items'][i]['snippet']['title'] == categoryName:
			categoryID=videoCategories['items'][i]['id']
			print('category found, id: '+categoryID)
			break
		else:
			i+=1
			continue
	if categoryID=='0':
		print('category not found, heres the current category list:'+videoCategories)
		exit()
	
	request = youtube.videos().insert(
		part="snippet,status",
		notifySubscribers=True,
		body={
		  "snippet": {
		    "categoryId": categoryID,
		    "description": description,
		    "title": title,
			"tags": tags
		  },
		  "status": {
		    "privacyStatus": privacyStatus
		  }
		},
		media_body=MediaFileUpload(vidFile,mimetype='video/mp4',resumable=True)
	)
	response=None
	while response is None:
		try:		
			status,response=request.next_chunk()
			if status:
				print(f'Uploaded {status.progress()*100}')
			print(response)
		except HttpError as e:
			if e.resp.status in [500,502,503,504]:
				continue

	return response